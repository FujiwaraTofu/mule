@testset "Slab Comparison" begin
    N = 8
    J = 100
    E = J + 1

    h = mu.gen_step(J, 0.25, 0.1, 0.1)
    H = sum(h)
    c = mu.get_centers(h)

    Σt  = @. 1. + 0.1*sin(c / H)
    Σs  = @. 0.5 + 0.25*cos(c / H)
    χ   = fill(1., J)
    νΣf = @. 2. + sin(2 * pi * c / H)
    Q   = @. 3. + c / H

    μs_r, ws_r = sl.gauss_legendre(N)

    ψlb_in = @. 2. + μs_r^2
    ψrb_in = @. 3. - μs_r
    ilb = mu.ITB(reshape(ψlb_in, div(N,2), 1))
    irb = mu.ITB(reshape(ψrb_in, div(N,2), 1))
    rlb = mu.RTB()
    rrb = mu.RTB()
    ψrlbg = @. 3. - 0.5 * μs_r^2

    bcs_coll = [(ilb,irb),
                (rlb,rrb),
                (ilb,rrb),
                (rlb,irb)]

    sl_ϕss = []
    mu_ϕss = []

    sl_Jss = []
    mu_Jss = []

    sl_Bss = []
    mu_Bss = []

    for bcs in bcs_coll
        mu_lb, mu_rb = bcs

        K = 1
        G = 1
        tp = mu.TP(N, K, G, J,
                  h,
                  reshape(Σt , G, J),
                  reshape(Σs , K, G, G, J),
                  reshape(χ  , G, J),
                  reshape(νΣf, G, J),
                  reshape(Q  , G, J),
                  mu_lb, mu_rb)

        out_mu = let
            if isa(mu_lb, mu.RTB) && isa(mu_rb, mu.RTB)
                out_mu = mu.transport_sweep(tp, reshape(Q, 1, 1, J);
                                            ψ_out_b_est = reshape(ψrlbg, div(N,2),1))
            else
                out_mu = mu.transport_sweep(tp, reshape(Q, 1, 1, J));
            end
            out_mu
        end

        out_sl = let
            mesh = sl.Mesh(sl.Cell.(h))
            ν = 2.3
            mats = sl.Material.(Σt, Σs, νΣf ./ ν, ν, Q)
            slvs = sl.FD.(fill(sl.alpha_dd, J))
            srcs = sl.Isotropic.((1 / 2) * Q)
            lb = co.gen_sl_bc(tp, mu_lb)[1]
            rb = co.gen_sl_bc(tp, mu_rb)[1]

            if isa(lb, sl.Reflective) && isa(rb, sl.Reflective)
                sl.process_outgoing_flux!(lb, ψrlbg)
            end

            # NOTE : Slab does not properly handle the Reflective-Incident case!
            # (need to sweep an extra time to recover correct left reflective
            # boundary behavior)
            ns = (isa(lb, sl.Reflective) && isa(rb, sl.Incident)) ? 2 : 1

            out_sl = nothing
            for i in 1:ns
                save_funs! = [sl.cell_scalar_flux_save_fun!,
                              sl.face_current_save_fun!,
                              sl.boundary_correction_save_fun!]
                save_datas = [sl.cell_save_data_init(mesh),
                              sl.face_save_data_init(mesh),
                              sl.s_face_save_data_init(mesh)]
                sl.sweep(mesh, mats, slvs, srcs,
                        (lb, rb),
                        μs_r, ws_r;
                        save_funs! = save_funs!,
                        save_datas = save_datas);
                out_sl = save_datas
            end
            out_sl
        end

        ϕsl, Jsl, Bsl = out_sl;
        Bsl_lb = Bsl[1]
        Bsl_rb = Bsl[E]

        cent_mu, edge_mu = out_mu
        ϕmu = cent_mu[1, 1, :]
        Jmu = edge_mu[1, 1, :]
        Bmu = edge_mu[2, 1, :]
        Bmu_lb = Bmu[1]
        Bmu_rb = Bmu[E]

        push!(sl_ϕss, ϕsl)
        push!(sl_Jss, Jsl)
        push!(sl_Bss, (Bsl_lb, Bsl_rb))

        push!(mu_ϕss, ϕmu)
        push!(mu_Jss, Jmu)
        push!(mu_Bss, (Bmu_lb, Bmu_rb))
    end

    for (i, bcs) in enumerate(bcs_coll)
        @test all(isapprox.(sl_ϕss[i], mu_ϕss[i]))
        @test all(isapprox.(sl_Jss[i], mu_Jss[i]))
        @test all(isapprox.(sl_Bss[i], mu_Bss[i]))
    end
end
