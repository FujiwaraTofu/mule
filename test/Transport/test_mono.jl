@testset "Slab Comparison" begin
    N = 16
    K = 1
    G = 1

    H = 2.5
    Δ = 0.025
    J = Int(H / Δ)

    h = fill(Δ, J)
    c = mu.get_centers(h)

    Σt = 1.0 .+ 0.05 * sin.(2 * pi * c / H)
    Σs = 0.4 .+ 0.05 * cos.(2 * pi * c / H)
    Σf = 0.2 .+ 0.10 * cos.(2 * pi * c / H)
    ν  = 0.05
    # NOTE : Q will be defined later

    νΣf = ν .* Σf
    χ = fill(1., J)

    μs_r, ws_r = sl.gauss_legendre(N)
    ψi_l = 15 .+ cos.(μs_r)
    ψi_r = 35 .+ μs_r

    re_lb = mu.RTB()
    re_rb = mu.RTB()
    in_lb = mu.ITB(reshape(ψi_l, div(N, 2), 1))
    in_rb = mu.ITB(reshape(ψi_r, div(N, 2), 1))
    va_lb = mu.ITB(zeros(div(N, 2), 1))
    va_rb = mu.ITB(zeros(div(N, 2), 1))

    bcs_rr = (re_lb, re_rb)
    bcs_ii = (in_lb, in_rb)
    bcs_ri = (re_lb, in_rb)
    bcs_ir = (in_lb, re_rb)
    bcs_vv = (va_lb, va_rb)
    bcs_rv = (re_lb, va_rb)
    bcs_vr = (va_lb, re_rb)

    bcs_fs = [bcs_rr, bcs_ii, bcs_ri, bcs_ir]
    bcs_ei = [bcs_rr, bcs_vv, bcs_rv, bcs_vr]

    # NOTE : fixed-source
    Q  = 3.0 .+ 0.15 * c
    for (lb, rb) in bcs_fs
        tp = mu.TP(N, K, G, J,
                   h,
                   reshape(Σt , G, J),
                   reshape(Σs , K, G, G, J),
                   reshape(χ  , G, J),
                   reshape(νΣf, G, J),
                   reshape(Q  , G, J),
                   lb, rb)

        # Mule solution (Linear System)
        m_T, m_Q = mu.const_transp_fixed_sys(tp)
        C   = mu.const_transp_C(tp)
        D_1 = mu.const_transp_D(tp; K = 1)

        mu_ϕ_sol = D_1*C*(m_T \ m_Q)

        # Slab solution
        sl_ϕss = co.sl_mono_si(tp)
        sl_ϕ_sol = sl_ϕss[end]

        @test all(isapprox.(mu_ϕ_sol, sl_ϕ_sol))

        # Mule solution (Sweeping / Source Iteration)
        mu_si_ϕss = mu.source_iteration(tp)
        mu_si_ϕ_sol = mu_si_ϕss[end]
        @show size(mu_si_ϕ_sol)
    end

    # NOTE : eigenvalue
    Q  = zeros(J)
    for (lb, rb) in bcs_ei
        tp = mu.TP(N, K, G, J,
                   h,
                   reshape(Σt , G, J),
                   reshape(Σs , K, G, G, J),
                   reshape(χ  , G, J),
                   reshape(νΣf, G, J),
                   reshape(Q  , G, J),
                   lb, rb)

        # Mule solution
        m_R, m_U = mu.const_transp_eig_sys(tp)
        C   = mu.const_transp_C(tp)
        D_1 = mu.const_transp_D(tp; K = 1)
        apk_λ, apk_ψ = Arpack.eigs(m_R, m_U; nev=1, which=:SM)
        mu_λ_sol = apk_λ[1].re
        mu_ψ_sol = [ψ.re for ψ in apk_ψ[:,1]]
        mu_ϕ_sol = D_1 * C * mu_ψ_sol
        # renormalize
        if ! all_equal(sign.(mu_ϕ_sol))
            error()
        end
        mu_ϕ_sol *= sign(first(mu_ϕ_sol)) * mu.def_nsf(mu_ϕ_sol)

        # Slab solution
        sl_ϕss, sl_λss = co.sl_mono_si(tp;
                                       eigenvalue = true,
                                       nsf        = co.def_sl_nsf)
        sl_ϕ_sol = sl_ϕss[end]
        sl_λ_sol = sl_λss[end]

        @test all(isapprox.(mu_ϕ_sol, sl_ϕ_sol))
        @test isapprox(mu_λ_sol, sl_λ_sol)
    end
end # "Slab Comparison"
