@testset "Slab Comparison" begin
    # NOTE : problem parameter construction (TransportProblem)
    N = 12
    K = 1
    G = 3

    H = 2.5
    Δ = 0.025
    J = Int(H / Δ)

    plt_shp(ϕ) = permutedims(reshape(ϕ, G, J))

    h = fill(Δ, J)
    c = mu.get_centers(h)

    Σt1 = @. 1.0 + 0.05 * sin(2 * pi * c / H)
    Σt2 = @. 2.0 + 0.10 * cos(4 * pi * c / H)
    Σt3 = @. 4.0 + 0.15 * sin(1 * pi * c / H)
    Σt = permutedims([Σt1 Σt2 Σt3])

    Σs11 = @. 0.2 .+ 0.05 * (c / H)
    Σs12 = @. 0.1 .+ 0.10 * (c / H)^2
    Σs13 = fill(0.2, J)

    Σs21 = @. 0.3 .+ 0.10 * sin(pi * c / H)
    Σs22 = fill(0.1, J)
    Σs23 = @. 0.1 .+ 0.05 * cos(3 * pi * c / H)

    Σs31 = fill(0.1, J)
    Σs32 = fill(0.2, J)
    Σs33 = fill(0.4, J)

    Σs = reshape(vec(permutedims([Σs11 Σs21 Σs31 Σs12 Σs22 Σs32 Σs13 Σs23 Σs33])),
                  1, G, G, J)

    χ1 = @. 0.5 + 0.10 * sin(pi * c / H)
    χ2 = @. 0.1 + 0.05 * cos(3 * pi * c / H)
    χ3 = @. 1 - χ1 - χ2
    χ = permutedims([χ1 χ2 χ3])

    ν = 2.3

    Σf1 = @. 0.2 + 0.1 * c / H
    Σf2 = @. 0.3 + 0.2 * (c / H)^3
    Σf3 = fill(0.1, J)

    Σf  = permutedims([Σf1 Σf2 Σf3])
    νΣf = permutedims(ν .* [Σf1 Σf2 Σf3])

    μs_rg, _ = sl.gauss_legendre(N)
    ψ1_lb = @. 1. + μs_rg
    ψ2_lb = @. 2. + cos(2 * pi * μs_rg)
    ψ3_lb = @. 4. + (μs_rg)^2 * cos(pi * μs_rg)
    ψ_lb = [ψ1_lb ψ2_lb ψ3_lb]

    μs_lg = - μs_rg
    ψ1_rb = @. 2. - μs_lg^3
    ψ2_rb = @. 3. + cos(μs_lg) * sin(2 * π * μs_lg)
    ψ3_rb = @. 5. - μs_lg
    ψ_rb = [ψ1_rb ψ2_rb ψ3_rb]

    re_lb = mu.RTB()
    re_rb = mu.RTB()
    in_lb = mu.ITB(ψ_lb)
    in_rb = mu.ITB(ψ_rb)
    va_lb = mu.ITB(zeros(div(N,2), G))
    va_rb = mu.ITB(zeros(div(N,2), G))

    bcs_rr = (re_lb, re_rb)
    bcs_ii = (in_lb, in_rb)
    bcs_ri = (re_lb, in_rb)
    bcs_ir = (in_lb, re_rb)
    bcs_vv = (va_lb, va_rb)
    bcs_rv = (re_lb, va_rb)
    bcs_vr = (va_lb, re_rb)

    bcs_fs = (bcs_rr, bcs_ii, bcs_ri, bcs_ir)
    bcs_ei = (bcs_rr, bcs_vv, bcs_rv, bcs_vr)

    # NOTE : fixed-source
    # NOTE : finite inhomogeneous source for fixed-source problems
    Q1 = @. 3 - 1 * (c / H)^4
    Q2 = @. 2 + 1 * (c / H)^3 + 0.5 * sin(pi * c / H)
    Q3 = fill(0.5, J)
    Q = permutedims([Q1 Q2 Q3])
    for (lb, rb) in bcs_fs
        tp = mu.TP(N, K, G, J,
                  h, Σt, Σs, χ, νΣf, Q,
                  lb, rb)

        m_T, m_Q = mu.const_transp_fixed_sys(tp)
        C   = mu.const_transp_C(tp)
        D_1 = mu.const_transp_D(tp; K=1)
        mu_ϕ_sol = plt_shp(reshape(D_1 * C * (m_T \ m_Q), G, J))

        sl_ϕ_sol = plt_shp(co.sl_mg_si(tp)[end])

        @test all(isapprox.(mu_ϕ_sol, sl_ϕ_sol))
    end

    # NOTE : eigenvalue
    # NOTE : zeroed inhomogeneous source for eigenvalue problems
    Q = zeros(G, J)
    for (lb, rb) in bcs_ei
        tp = mu.TP(N, K, G, J,
                  h, Σt, Σs, χ, νΣf, Q,
                  lb, rb)

        sl_ϕ_save, sl_λ_save = co.sl_mg_si(tp; eigenvalue=true)
        sl_ϕ_sol = plt_shp(sl_ϕ_save[end])
        sl_λ_sol = sl_λ_save[end]

        m_R, m_U = mu.const_transp_eig_sys(tp)
        C   = mu.const_transp_C(tp)
        D_1 = mu.const_transp_D(tp; K = 1)
        apk_λ, apk_ψ = Arpack.eigs(m_R, m_U; nev=1, which=:SM)
        mu_λ_sol = apk_λ[1].re
        mu_ψ_sol = [ψ.re for ψ in apk_ψ[:,1]]
        mu_ϕ_sol = D_1 * C * mu_ψ_sol
        # renormalize
        if ! all_equal(sign.(mu_ϕ_sol))
            error()
        end
        mu_ϕ_sol *= sign(first(mu_ϕ_sol)) * (1 / norm(mu_ϕ_sol, 2))
        mu_ϕ_sol = plt_shp(mu_ϕ_sol)

        @test all(isapprox.(mu_ϕ_sol, sl_ϕ_sol))
        @test all(isapprox.(mu_λ_sol, sl_λ_sol))
    end
end
