@testset "Shift Methodology" begin
    # spectral radius is expected to be equal to the dominance ratio (|λ_1|
    # / |λ_2|) where λ_i is the i'th smallest eigenvalue

    # multigroup diffusion eigenvalue problem guarantees that the eigenspace
    # of the smallest eigenvalue is spanned by a single eigenvector

    H  = 10.
    Δz =  0.05
    J = Int(H / Δz)
    h = fill(Δz, J)

    c = Mule.get_centers(h)

    G = 3

    Σt  = [2., 3., 1.]
    D   = 1 ./ (3 * Σt)
    Σs  = [0.5 0.2 0.1;
           0.0 0.2 0.4;
           0.0 0.0 0.8]
    χ   = [0.5, 0.3, 0.2]
    νΣf = [1.0, 1.5, 1.0]
    Q   = zeros(G)

    lb = Mule.RDB()
    rb = Mule.MDB(zeros(G))

    dp = Mule.DP(G, J,
                 h,
                 D, Σt, Σs, χ, νΣf, Q,
                 lb, rb)

    M, F = Mule.const_diffus_eigen_sys(dp)

    # determine baseline solutions (using Julia's internal eigen-solver)
    et = eigen(Matrix(M), Matrix(F))
    λt_1, λt_2 = sort(et.values, by=abs)[1:2]
    λt_1_idx = argmin(abs.(et.values))
    ϕt_1 = et.vectors[:, λt_1_idx]
    # NOTE : re-normalize to be same as power_iterate function
    ϕt_1 *= 1 / norm(ϕt_1, 2)

    # unshifted
    λs_funs_unshifted = [Mule.λs_fun_const]
    ρts_unshifted      = [abs(λt_1) / abs(λt_2)]

    c0s_slab = [0.1, 0.2, 0.5, 0.75]
    λs_funs_slab = (
        [Mule.gen_λs_fun_slab(c0, Mule.def_slab_c1, Mule.def_slab_λ_min)
          for c0 in c0s_slab]
    )
    ρts_slab = [abs(λt_1 - c0*λt_1) / abs(λt_2 - c0*λt_1) for c0 in c0s_slab]

    c0s_parcs = [0.05, 0.10, 0.15]
    λs_funs_parcs = (
        [Mule.gen_λs_fun_parcs(c0, Mule.def_parcs_c1, Mule.def_parcs_λ_min)
          for c0 in c0s_parcs]
    )
    ρts_parcs = [abs(λt_1 - (λt_1 - c0)) / abs(λt_2 - (λt_1 - c0)) for c0 in c0s_parcs]

    λs_funs = [λs_funs_unshifted; λs_funs_slab; λs_funs_parcs]
    ρts     = [ρts_unshifted; ρts_slab; ρts_parcs]

    for (λs_fun, ρt) in zip(λs_funs, ρts)
        ϕs_save, λs_save = (
            Mule.power_iterate(M, F; save=true, λs_fun = λs_fun)
        )
        ϕs = ϕs_save[end]
        λs = λs_save[end]
        ρs = Mule.est_rho(ϕs_save)

        @test all(isapprox.(ϕs, ϕt_1))
        @test isapprox(λs, λt_1)
        @test isapprox(ρs, ρt; rtol=1.e-2)
    end
end # "Shift Methodology"
