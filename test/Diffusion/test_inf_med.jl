# M ϕ = Q
@testset "Inhomogeneous" begin
    G = 3 # num. groups
    J = 5 # num. identical cells

    # NOTE : mesh spacing shouldn't actually matter (flat solution)
    h = rand(J)

    # macroscopic total cross-section (indices → g ≡ group index.)
    Σt_1 = 1.0
    Σt_2 = 2.0
    Σt_3 = 5.0
    Σt = [Σt_1, Σt_2, Σt_3]
    # diffusion coefficients (assume isotropic scattering)
    D = 1 ./ (3 * Σt)

    # differential macroscopic scattering cross-section (indices g', g ≡
    # out-scatter group, in-scatter group
    Σs_1_1 = 0.1; Σs_1_2 = 0.2; Σs_1_3 = 0.4
                  Σs_2_2 = 0.5; Σs_2_3 = 0.7
                                Σs_3_3 = 0.9
    Σs = [Σs_1_1 Σs_1_2 Σs_1_3;
          0.0    Σs_2_2 Σs_2_3;
          0.0    0.0    Σs_3_3]

    # inhomogeneous source
    Q_1 = 0.5
    Q_2 = 1.5
    Q_3 = 0.1
    Q = [Q_1, Q_2, Q_3]

    # zeroed out parameters
    χ   = zeros(G)
    νΣf = zeros(G)

    lb = Mule.RDB()
    rb = Mule.RDB()

    dp = Mule.DiffusionProblem(G, J,
                               h,
                               D, Σt, Σs, χ, νΣf, Q,
                               lb, rb)

    d_M, d_Q = Mule.const_diffus_fixed_sys(dp)

    sol = reshape(d_M \ d_Q, G, J)

    # theoretical solution
    the = (Diagonal(Σt) - transpose(Σs)) \ Q

    # test flat solution spatially
    for g in 1:G
        @test all_isapprox(sol[g,:])
    end

    j = 1
    @test all(isapprox.(sol[:,j], the))
end # "Inhomogeneous"

# M ϕ = F ϕ + Q
# M ϕ = λ F ϕ
@testset "Fissionable" begin
    G = 2 # num. groups
    J = 5 # num. identical cells

    # NOTE : mesh spacing shouldn't actually matter (flat solution)
    h = rand(J)

    # macroscopic total cross-section (indices → g ≡ group index.)
    Σt_1 = 1.
    Σt_2 = 1.5
    Σt = [Σt_1, Σt_2]
    # diffusion coefficients (assume isotropic scattering)
    D = 1 ./ (3 * Σt)

    # differential macroscopic scattering cross-section (indices g', g ≡
    # out-scatter group, in-scatter group
    Σs_1_1 = 0.1; Σs_1_2 = 0.2
                  Σs_2_2 = 1.0
    Σs = [Σs_1_1 Σs_1_2;
          0.0    Σs_2_2]

    # fission source distribution
    χ = [0.8, 0.2]

    # neutron multiplicity times macroscopic fission cross-section
    νΣf = [0.1, 0.5]

    # inhomogeneous source
    Q = [1., 2.]

    lb = Mule.RDB()
    rb = Mule.RDB()

    dp = Mule.DP(G, J,
                 h, D, Σt, Σs, χ, νΣf, Q,
                 lb, rb)

    # Mule operators
    d_M = Mule.const_diffus_M(dp)
    d_F = Mule.const_diffus_F(dp)
    d_Q = Mule.const_diffus_Q(dp)

    sol = reshape((d_M - d_F) \ d_Q, G, J)

    # theoretical solution
    the = (Diagonal(Σt) - transpose(Σs) - (χ * transpose(νΣf))) \ Q

    # test flat solution spatially
    for g in 1:G
        @test all_isapprox(sol[g,:])
    end

    j = 1
    @test all(isapprox.(sol[:,j], the))

    # NOTE : testing infinite medium λ-eigenvalue (removing inhomogeneous
    # source)
    λ_sol = min(eigvals(Matrix(d_M), Matrix(d_F))...)

    # NOTE : theoretical infinite medium eigenvalue (λ_{∞}) this expression
    # was taken from Ben Yee's Nuclear Engineering and Technology MSED paper
    # (Nuclear Engineering and Technology 49 (2017) 1125-1134) equation (14)
    λ_the = 1 / (transpose(νΣf) * ((Diagonal(Σt) - transpose(Σs)) \ χ))

    @test isapprox(λ_sol, λ_the)
end # "Fissionable"
