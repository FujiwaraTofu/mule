# NOTE : MMS is most easily applied to Fixed-Source problems (at the moment,
# that is all that is done)

# M ϕ = Q
@testset "Inhomogeneous" begin
    @testset "refl., refl." begin
        Δzs = 1 ./ (2 .^ (0:7))
        nes = VF(undef, length(Δzs))

        for (i, Δz) in enumerate(Δzs)
            G = 2

            H = 10.
            J  = Int(H / Δz)
            h  = fill(Δz, J)
            c  = Mule.get_centers(h)

            Σt = [1., 3.]
            D  = 1 ./ (3 * Σt)
            Σs = [1/2. 1/5.;
                  1/3. 1.   ]
            χ   = zeros(G)
            νΣf = zeros(G)

            Q1 = @. 1/6*(7+(3+(2*pi^2)/H^2)*cos((c*pi)/H)-2*cos((2*c*pi)/H))
            Q2 = @. 7-1/5*cos((c*pi)/H)+(2+(4*pi^2)/(9*H^2))*cos((2*c*pi)/H)
            Q = permutedims([Q1 Q2])

            lb = Mule.RDB()
            rb = Mule.RDB()

            dp = Mule.DP(G, J,
                         h,
                         repeat(D  ; outer=(1,J)),
                         repeat(Σt ; outer=(1,J)),
                         repeat(Σs ; outer=(1,1,J)),
                         repeat(χ  ; outer=(1,J)),
                         repeat(νΣf; outer=(1,J)),
                         Q,
                         lb, rb)

            d_M, d_Q = Mule.const_diffus_fixed_sys(dp)
            ϕs = permutedims(reshape(d_M \ d_Q, G, J))

            # mms theoretical values
            ϕ1t = @. cos((pi*c)/H) + 5
            ϕ2t = @. cos((2*pi*c/H)) + 4
            ϕt = [ϕ1t ϕ2t]

            nes[i] = norm(vec(ϕt .- ϕs), Inf)

            if i == length(Δzs)
                @test all(isapprox.(ϕs, ϕt, rtol=1.e-6))
            end
        end

        nrs = nes[1:end-1] ./ nes[2:end]
        @test all(isapprox.(nrs[2:end], 4; atol=0.05))
    end # "refl., refl."

    @testset "marsh., marsh." begin
        Δzs = 1 ./ (2 .^ (0:8))
        nes = VF(undef, length(Δzs))

        for (i, Δz) in enumerate(Δzs)
            G = 2

            H = 10.
            J  = Int(H / Δz)
            h  = fill(Δz, J)
            c  = Mule.get_centers(h)

            Σt = [1/6, 1/9]
            D  = 1 ./ (3 * Σt)
            Σs = [1/7 1/5;
                  1/2 1/4]
            χ   = zeros(G)
            νΣf = zeros(G)

            Q1 = @. -(1/(84*H^4))*(21*exp(c/H)*c^3*H+(38+21*c)*H^4-2*exp(c^2/H^2)*((-336*c^2+H^2*(-168+H^2+336*pi^2))*cos((2*c*pi)/H)+672*c*H*pi*sin((2*c*pi)/H)))
            Q2 = @. 1/360*(-194-25*c-(5*exp(c/H)*c*(648*c*H+648*H^2+c^2*(108+5*H^2)))/H^5-72*exp(c^2/H^2)*cos((2*c*pi)/H))
            Q = permutedims([Q1 Q2])

            Jml = [3/4, -1/2]
            Jmr = [1/4*(2+exp(1)+(8*exp(1))/H), 1/4*(1+exp(1)/2+6*(1/2+(2*exp(1))/H)+H/2)]
            lb = Mule.MDB(Jml)
            rb = Mule.MDB(Jmr)

            dp = Mule.DP(G, J,
                         h,
                         repeat(D  ; outer=(1,J)),
                         repeat(Σt ; outer=(1,J)),
                         repeat(Σs ; outer=(1,1,J)),
                         repeat(χ  ; outer=(1,J)),
                         repeat(νΣf; outer=(1,J)),
                         Q,
                         lb, rb)

            d_M, d_Q = Mule.const_diffus_fixed_sys(dp)
            ϕs = permutedims(reshape(d_M \ d_Q, G, J))

            # mms theoretical values
            ϕ1t = @. 2+exp((c/H)^2)*cos((2*pi*c)/H)
            ϕ2t = @. 1+1/2*(c/H)^3*exp(c/H)+1/2*c
            ϕt = [ϕ1t ϕ2t]

            nes[i] = norm(vec(ϕt .- ϕs), Inf)

            if i == length(Δzs)
                @test all(isapprox.(ϕs, ϕt, rtol=1.e-5))
            end
        end

        nrs = nes[1:end-1] ./ nes[2:end]
        @test all(isapprox.(nrs[3:end], 4; atol=0.05))
    end # "marsh., marsh."
end # "Inhomogeneous"

# M ϕ = F ϕ + Q
@testset "Fissionable" begin
    @testset "period., period." begin
        Δzs = (1 / 2^2) ./ (2 .^ (0:6))
        nes = VF(undef, length(Δzs))

        for (i, Δz) in enumerate(Δzs)
            G = 3

            H = 10.
            J  = Int(H / Δz)
            h  = fill(Δz, J)
            c  = Mule.get_centers(h)

            Σt = permutedims([(@. 2 + cos((2*pi*c)/H)) fill(2., J) fill(3., J)])
            D  = 1 ./ (3 * Σt)
            Σs = cat([transpose([(1/2)  (1/2)                      (@. 1 + cos((2*pi*z)/H));
                                 (3/2)  (1/8)                      (0.);
                                 (0.)   (@. 4. + cos((4*pi*z)/H))  (1.)])
                      for z in c]..., dims=3)
            χ = permutedims([fill(1/10, J) fill(3/10, J) (@. 4/10 + 1/10*cos((2*pi*c)/H))])
            νΣf = permutedims([(@. 1+1/5*cos((4*pi*(c - H/4))/H)) fill(2., J) fill(3., J)])

            Q1 = @. 7/10*(-2+cos((1/5+(4*c)/H)*pi))+(-(13/10)-cos((2*c*pi)/H))*(3+cos((6*c*pi)/H))-1/50*(70+50*cos((2*c*pi)/H)+cos((4*c*pi)/H))*(-4+cos(((6*c+H)*pi)/(3*H)))-(4*pi^2*cos(((6*c+H)*pi)/(3*H)))/(3*H^2*(2+cos((2*c*pi)/H)))+(pi^2*(-1+2*sin((1/6-(4*c)/H)*pi)))/(3*H^2*(2+cos((2*c*pi)/H))^2)
            Q2 = @. -((8*pi^2*cos((1/5+(4*c)/H)*pi))/(3*H^2))-9/10*(3+cos((6*c*pi)/H))+3/50*(-30+cos((4*c*pi)/H))*(4+cos((2*(c-H/3)*pi)/H))+51/40*(2+cos((4*(c-H/5)*pi)/H))
            Q3 = @. 1/50*(10*(-2+cos((1/5+(4*c)/H)*pi))*(24+cos((2*c*pi)/H)+5*cos((4*c*pi)/H))+(200*pi^2*cos((6*c*pi)/H))/H^2-5*(-8+3*cos((2*c*pi)/H))*(3+cos((6*c*pi)/H))+(4+cos((2*c*pi)/H))*(-5+cos((4*c*pi)/H))*(4+cos((2*(c-H/3)*pi)/H)))
            Q = permutedims([Q1 Q2 Q3])

            lb = Mule.PDB()
            rb = Mule.PDB()

            dp = Mule.DP(G, J,
                         h,
                         repeat(D  ; outer=(1,J)),
                         repeat(Σt ; outer=(1,J)),
                         repeat(Σs ; outer=(1,1,J)),
                         repeat(χ  ; outer=(1,J)),
                         repeat(νΣf; outer=(1,J)),
                         Q,
                         lb, rb)


            d_M = Mule.const_diffus_M(dp)
            d_F = Mule.const_diffus_F(dp)
            d_Q = Mule.const_diffus_Q(dp)

            ϕs = transpose(reshape((d_M - d_F) \ d_Q, G, J))

            ϕ1t = @. 4 + cos((2*pi*(c-H/3))/H)
            ϕ2t = @. 2 + cos((4*pi*(c-H/5))/H)
            ϕ3t = @. 3 + cos((6*pi*c)/H)
            ϕt = [ϕ1t ϕ2t ϕ3t]

            nes[i] = norm(vec(ϕt .- ϕs), Inf)

            if i == length(Δzs)
                @test all(isapprox.(ϕs, ϕt, rtol=1.e-6))
            end
        end

        nrs = nes[1:end-1] ./ nes[2:end]
        @test all(isapprox.(nrs[end-2:end], 4; atol=0.10))
    end # "period., period."
end # "Fissionable"
