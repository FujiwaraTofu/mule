using Test

import Mule
import Mule.Compat
import Slab

using Mule.Types

using Mule: est_rho

using  LinearAlgebra
import Arpack

# NOTE : module aliases
mu = Mule
co = Compat
sl = Slab

# NOTE : test helper functions

all_equal(coll) = all([elem == first(coll) for elem in coll])
# NOTE : not completely correct since we only compare to the first element in
# the collection
all_isapprox(coll; kwargs...) = begin
    return all([isapprox(elem, first(coll); kwargs...) for elem in coll])
end

# @testset "Diffusion" begin
#     diff_dir = "Diffusion"

#     @testset "Infinite Medium" begin
#         include(joinpath(diff_dir, "test_inf_med.jl"))
#     end
#     @testset "MMS" begin
#         include(joinpath(diff_dir, "test_mms.jl"))
#     end
#     @testset "Power Iteration" begin
#         include(joinpath(diff_dir, "test_power.jl"))
#     end
# end

@testset "Transport" begin
    tran_dir = "Transport"

    @testset "Monoenergetic" begin
        include(joinpath(tran_dir, "test_mono.jl"))
    end
    # @testset "Multigroup" begin
    #     include(joinpath(tran_dir, "test_multi.jl"))
    # end
    # @testset "Sweep" begin
    #     include(joinpath(tran_dir, "test_sweep.jl"))
    # end
end
