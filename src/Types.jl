# NOTE : type definitions
module Types

export F, VF, MF
export VI, MI
export Fun, VFun

F = Float64
VF = Vector{F}
MF = Matrix{F}

VI = Vector{Int}
MI = Matrix{Int}

Fun  = Function
VFun = Array{<:Fun, 1}

end # Types
