# NOTE : compatibility module for testing Mule implementation compared to
# mono-energetic Slab
module Compat

import Slab
import Mule

using Mule.Types

using LinearAlgebra

sl = Slab
mu = Mule

gen_sl_bc(tp::mu.TP, mu_bc::mu.RTB) = begin
    N = mu.get_N(tp)
    G = mu.get_G(tp)
    return [sl.Reflective(zeros(div(N, 2))) for _ in 1:G]
end
gen_sl_bc(tp::mu.TP, mu_bc::mu.ITB) = begin
    G = mu.get_G(tp)
    return [sl.Incident(mu_bc.ψm[:, g]) for g in 1:G]
end

# NOTE : generate an inhomogeneous source consistent with the provided current
# scalar flux estimate
# NOTE : ONLY isotropic scattering supported by Slab
# ϕs : G x J
function gen_S(tp::mu.TP, ϕs::Array{F, 2}; λ::F = 1.)
    K = mu.get_K(tp)
    G = mu.get_G(tp)
    J = mu.get_J(tp)
    if K != 1
        error("Slab only supports isotropic scattering")
    end

    S = zeros(G, J)

    for j in 1:J
        Σs  = permutedims(tp.Σs[K, 1:G, 1:G, j])
        χ   = tp.χ[1:G, j]
        νΣf = reshape(tp.νΣf[1:G, j], 1, G)
        Q   = tp.Q[1:G, j]
        ϕ   = ϕs[1:G, j]

        S[1:G, j] = (Σs + λ*χ*νΣf)*ϕ + Q
    end

    return S
end

# ϕs : G x J
# Jl : G
# Jr : G
function est_λ(tp::mu.TP, ϕs::Array{F,2}, Jl::Array{F,1}, Jr::Array{F,1})
    K = mu.get_K(tp)
    G = mu.get_G(tp)
    J = mu.get_J(tp)
    # TODO : if higher-legendre polynomial orders integrate to zero in Mule,
    # this shouldn't be an issue
    if K != 1
        error("Slab only supports isotropic scattering")
    end

    λ_num = 0. # mutated
    λ_den = 0. # mutated
    for j in 1:J
        Δz  = tp.h[j]
        Σt  = tp.Σt[1:G, j]
        Σs  = permutedims(tp.Σs[K, 1:G, 1:G, j])
        χ   = tp.χ[1:G, j]
        νΣf = reshape(tp.νΣf[1:G, j], 1, G)
        ϕ   = ϕs[1:G, j]

        λ_num += sum((Diagonal(Σt) - Σs)*ϕ*Δz)
        λ_den += sum((χ*νΣf)*ϕ*Δz)
    end
    # boundary current contribution
    λ_num += sum(Jr) - sum(Jl)

    λ = λ_num / λ_den

    return λ
end

# default Slab initial scalar flux estimate
def_sl_ϕs_init(tp::mu.TP) = begin
    J = mu.get_J(tp)
    G = mu.get_G(tp)
    return repeat(permutedims(mu.def_vector_param(J)), outer=(G, 1))
end
# default Slab initial eigenvalue  estimate
def_sl_λ_init(tp) = begin
    return mu.def_scalar_param()
end

def_sl_mi  = Inf
def_sl_cf  = mu.def_cf
def_sl_nsf = (_mesh, _mats, ϕs) -> mu.def_nsf(ϕs)

gen_sl_mesh(tp::mu.TP) = sl.Mesh(sl.Cell.(tp.h))
gen_sl_slvs(tp::mu.TP) = sl.FD.(fill(sl.alpha_dd, mu.get_J(tp)))
gen_sl_quad(tp::mu.TP) = sl.gauss_legendre(mu.get_N(tp))

# NOTE : Slab, Fixed-Source, Soutce-Iteration (Monoenergetic)
function sl_mono_si(tp::mu.TP;
                    eigenvalue       = false,
                    nsf              = def_sl_nsf,
                    mi::Number       = def_sl_mi,
                    cf::Function     = def_sl_cf)
    G = mu.get_G(tp)
    if G != 1
        error("sl_fs_si requires single group energy structure")
    end

    mesh       = gen_sl_mesh(tp)
    slvs       = gen_sl_slvs(tp)
    μs_r, ws_r = gen_sl_quad(tp)

    lb = gen_sl_bc(tp, tp.lb)[G]
    rb = gen_sl_bc(tp, tp.rb)[G]

    Σt  = tp.Σt[1,:]
    Σs  = tp.Σs[1,1,1,:]
    νΣf = tp.νΣf[1,:]
    Q   = tp.Q[1,:]

    # TODO : make consistent material definitions
    J = mu.get_J(tp)
    ν_std = 2.3
    ν  = fill(ν_std, J)
    Σf = νΣf ./ ν

    mats = sl.Material.(Σt, Σs, Σf, ν, Q)

    if eigenvalue
        ϕss, λss = (
            sl.source_iteration(mesh, mats, slvs,
                                (lb, rb),
                                μs_r, ws_r;
                                eigenvalue     = true,
                                norm_scale_fun = nsf,
                                max_iters      = mi,
                                conv_fun       = cf)
        )
        return ϕss, λss
    else
        ϕss = sl.source_iteration(mesh, mats, slvs,
                                  (lb, rb),
                                  μs_r, ws_r;
                                  eigenvalue = false,
                                  max_iters  = mi,
                                  conv_fun   = cf)
        return ϕss
    end
end

function sl_mg_si(tp::mu.TP;
                  eigenvalue       = false,
                  nsf              = mu.def_nsf,
                  mi::Number       = def_sl_mi,
                  cf::Function     = def_sl_cf,
                  ϕs_0::Array{F,2} = def_sl_ϕs_init(tp),
                  λ_0::F           = def_sl_λ_init(tp))
    G = mu.get_G(tp)
    J = mu.get_J(tp)
    E = mu.get_E(tp)

    mesh       = gen_sl_mesh(tp)
    slvs       = gen_sl_slvs(tp)
    μs_r, ws_r = gen_sl_quad(tp)

    lb = gen_sl_bc(tp, tp.lb)
    rb = gen_sl_bc(tp, tp.rb)

    z_Σs = zeros(G, J)
    z_Σf = zeros(G, J)
    z_ν  = zeros(G, J)

    ϕss = []
    if eigenvalue
        λss = []
    end

    it = 0 # num. completed iterations
    ϕs_p = ϕs_0
    if eigenvalue
        λ_p = λ_0
    end
    while true
        if eigenvalue
            S_p    = gen_S(tp, ϕs_p; λ = λ_p)
        else
            S_p    = gen_S(tp, ϕs_p)
        end
        mats_p = sl.Material.(tp.Σt, z_Σs, z_Σf, z_ν, S_p)

        ϕs_n = Array{F, 2}(undef, G, J)
        Jl_n = Array{F, 1}(undef, G)
        Jr_n = Array{F, 1}(undef, G)
        # sweep each group independently
        for g in 1:G
            mats_g_p = mats_p[g, :]
            lb_g = lb[g]
            rb_g = rb[g]

            save_funs! = [sl.cell_scalar_flux_save_fun!,
                          sl.boundary_current_save_fun!]
            save_datas = [sl.cell_save_data_init(mesh),
                          sl.s_face_save_data_init(mesh)]
            sources_g_p = sl.fixed_source.(mats_g_p, ϕs_p[g, :])
            sl.sweep(mesh, mats_g_p, slvs, sources_g_p, (lb_g, rb_g), μs_r, ws_r;
                     save_funs! = save_funs!,
                     save_datas = save_datas)

            ϕs_n_g, Js_b_n_g = save_datas
            Jl_n_g = Js_b_n_g[1]
            Jr_n_g = Js_b_n_g[E]

            ϕs_n[g,:] = ϕs_n_g
            Jl_n[g] = Jl_n_g
            Jr_n[g] = Jr_n_g
        end

        if eigenvalue
            # eigenvalue update
            λ_n = est_λ(tp, ϕs_n, Jl_n, Jr_n)
            # re-normalization
            ϕs_n *= nsf(ϕs_n)
        end

        it += 1

        if eigenvalue
            converged = cf(ϕs_p, ϕs_n) && cf(λ_p, λ_n)
        else
            converged = cf(ϕs_p, ϕs_n)
        end
        iters_reached = it >= mi

        push!(ϕss, ϕs_n)
        if eigenvalue
            push!(λss, λ_n)
        end

        ϕs_p = ϕs_n
        if eigenvalue
            λ_p = λ_n
        end

        converged     ? (@goto label_converged)     : nothing
        iters_reached ? (@goto label_iters_reached) : nothing
    end
    @label label_converged
    @label label_iters_reached

    if eigenvalue
        return ϕss, λss
    else
        return ϕss
    end
end

end # Compat
