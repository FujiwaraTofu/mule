module Mule

using SparseArrays
using LinearAlgebra
using FastGaussQuadrature
using Jacobi: legendre

export minimum_by

import Base: length, iterate

include("Types.jl")
using Mule.Types

# iqc = iterated quantity collection
iter_rhos(iqc; order = 2) = begin
    n_iters      = length(iqc)
    n_diff_norms = n_iters - 1
    n_est_rhos   = n_diff_norms - 1
    diff_norms = [norm(iqc[i+1] - iqc[i], order) for i in 1:n_diff_norms]
    est_rhos   = [diff_norms[i+1] / diff_norms[i] for i in 1:n_est_rhos]
    return est_rhos
end

# minimum number of iters required to perform a linear regression on difference
# norms
MIN_REQ_ITERS_LIN = 3

# drop_front / drop_back : percentage of iterated collection to drop at the
# start and end for global spectral radius estimation
est_rho(iqc; order = 2, drop_front = 0.25, drop_back = 0.25) = begin
    # remove iqc iterated values that are to be dropped from the front and back
    n_iters = length(iqc)
    n_drop_front = Int(ceil(n_iters * drop_front))
    n_drop_back = Int(ceil(n_iters * drop_back))

    n_use = (n_iters - n_drop_front - n_drop_back)

    if n_use < MIN_REQ_ITERS_LIN
        error("insufficient number of iterations")
    end

    iqc_use = iqc[(n_drop_front + 1):(end - n_drop_back)]

    n_diff_norms_use = n_use - 1
    diff_norms_use = [norm(iqc_use[i+1] - iqc_use[i], order) for i in 1:n_diff_norms_use]

    slope, inter = [1:n_diff_norms_use ones(n_diff_norms_use)] \ log.(diff_norms_use)

    return exp(slope)
end

function minimum_by(fun::Function, coll)
    appl = fun.(coll)   # apply function to coll.
    midx = argmin(appl) # location of minimum element
    return coll[midx]
end

# NOTE : utility function to generate a step distribution
function gen_step(n::Int, nom::F, dec::F, inc::F)
    # TODO : move away from floating point arithmetic
    n_dec = Int(ceil(n / 2))
    n_inc = n - n_dec

    return [(nom - dec) * ones(n_dec); (nom + inc) * ones(n_inc)]
end

# NOTE : define a general initial quantity
DEF_PARAM_MAG = 1.0
DEF_PARAM_DEC_PERC = 0.10
DEF_PARAM_INC_PERC = 0.10

# NOTE : default vector parameter given as step function
def_vector_param(n::Int) = begin
    gen_step(n, DEF_PARAM_MAG,
             DEF_PARAM_DEC_PERC * DEF_PARAM_MAG,
             DEF_PARAM_INC_PERC * DEF_PARAM_MAG)
end
def_scalar_param() = DEF_PARAM_MAG

get_edges(h::VF) = begin
    return cumsum([0; h])
end
get_centers(h::VF) = begin
    edges = get_edges(h)
    return (edges[1:(end-1)] + edges[2:end]) ./ 2
end

abstract type Boundary end
abstract type DiffusionBoundary <: Boundary end
DB = DiffusionBoundary

struct ReflectiveDiffusionBoundary <: DiffusionBoundary end
RDB = ReflectiveDiffusionBoundary

struct MarshakDiffusionBoundary <: DiffusionBoundary
    Jm::Array{F, 1} # G
end
MDB = MarshakDiffusionBoundary

struct PeriodicDiffusionBoundary <: DiffusionBoundary end
PDB = PeriodicDiffusionBoundary

# G : number of groups
# J : number of cells
struct DiffusionProblem
    G :: Int
    J :: Int

    h   :: Array{F, 1} # J
    D   :: Array{F, 2} # G x J
    Σt  :: Array{F, 2} # G x J
    Σs  :: Array{F, 3} # G x G x J
    χ   :: Array{F, 2} # G x J
    νΣf :: Array{F, 2} # G x J
    Q   :: Array{F, 2} # G x J

    lb::DB
    rb::DB
end
DP = DiffusionProblem

# constructor for identical material properties in each spatial cell
function DiffusionProblem(G::Int, J::Int,
                          h   :: Array{F, 1},
                          D   :: Array{F, 1},
                          Σt  :: Array{F, 1},
                          Σs  :: Array{F, 2},
                          χ   :: Array{F, 1},
                          νΣf :: Array{F, 1},
                          Q   :: Array{F, 1},
                          lb::DB, rb::DB)
    return DiffusionProblem(G, J,
                            h,
                            cat(fill(D  , J)...; dims=2),
                            cat(fill(Σt , J)...; dims=2),
                            cat(fill(Σs , J)...; dims=3),
                            cat(fill(χ  , J)...; dims=2),
                            cat(fill(νΣf, J)...; dims=2),
                            cat(fill(Q  , J)...; dims=2),
                            lb, rb)
end

get_G(dp::DP) = dp.G
get_J(dp::DP) = dp.J

# return periodic neighbor cell
# slab geometry gives (1, J) as periodic neighbors
get_period_neighbor(dp::DP, j::Int) = begin
    J = get_J(dp)
    if j == 1
        return J
    elseif j == J
        return 1
    else
        error("cell $(j) is not a boundary cell ($(j) ∉ {1, $(J)})")
    end
end

gen_h_f(h_l::F, h_r::F) = begin
    return (1 / 2) * (h_l + h_r)
end

gen_D_f(h_l::F, h_r::F, D_l::F, D_r::F) = begin
    return (h_l + h_r) / ((h_l / D_l) + (h_r / D_r))
end

# NOTE : diffusion system defined as
# M ϕ = F ϕ + Q
# ϕ : scalar flux (vector)
# M : leakage + collision + scattering operator (matrix)
# F : fission operator (matrix)
# Q : inhomogeneous source (vector)

# NOTE : construct diffusion M : leakage + collision + scattering operator
function const_diffus_M(dp::DP)
    G = get_G(dp)
    J = get_J(dp)

    lidxs = LinearIndices((G, J))

    M = spzeros(G*J, G*J)

    for j in 1:J
        h_c = dp.h[j]
        if j != 1
          h_l = dp.h[j-1]
          h_lf = gen_h_f(h_l, h_c)
        end
        if j != J
          h_r = dp.h[j+1]
          h_rf = gen_h_f(h_c, h_r)
        end

        for g in 1:G
            lidx_g_c = lidxs[g, j]

            # NOTE : cell-edge current
            D_g_c = dp.D[g, j]

            # left-face
            if j != 1
                lidx_g_l = lidxs[g, j-1]

                D_g_l = dp.D[g, j-1]

                D_g_lf = gen_D_f(h_l, h_c, D_g_l, D_g_c)

                M[lidx_g_c, lidx_g_l] += - D_g_lf / (h_lf * h_c)
                M[lidx_g_c, lidx_g_c] += + D_g_lf / (h_lf * h_c)
            end

            # right-face
            if j != J
                lidx_g_r = lidxs[g, j+1]

                D_g_r = dp.D[g, j+1]

                D_g_rf = gen_D_f(h_c, h_r, D_g_c, D_g_r)

                M[lidx_g_c, lidx_g_r] += - D_g_rf / (h_rf * h_c)
                M[lidx_g_c, lidx_g_c] += + D_g_rf / (h_rf * h_c)
            end

            # NOTE : total interaction
            M[lidx_g_c, lidx_g_c] += dp.Σt[g, j]

            # NOTE : scattering
            for gp in 1:G
                lidx_gp_c = lidxs[gp, j]
                M[lidx_g_c, lidx_gp_c] += - dp.Σs[gp, g, j]
            end
        end
    end

    # boundary condition contribution

    for (j, b) in zip([1, J], [dp.lb, dp.rb])
        if     isa(b, PDB)
            # NOTE : periodic neighbor index
            # n → neighbor
            j_n = get_period_neighbor(dp, j)

            h_c = dp.h[j]
            h_n = dp.h[j_n]

            # bf → boundary face
            h_bf = gen_h_f(h_c, h_n)

            for g in 1:G
                lidx_g_c = lidxs[g, j]
                lidx_g_n = lidxs[g, j_n]

                D_g_c = dp.D[g, j]
                D_g_n = dp.D[g, j_n]

                D_g_bf = gen_D_f(h_c, h_n, D_g_c, D_g_n)

                M[lidx_g_c, lidx_g_c] += + D_g_bf / (h_bf * h_c)
                M[lidx_g_c, lidx_g_n] += - D_g_bf / (h_bf * h_c)
            end
        elseif isa(b, MDB)
            h_c = dp.h[j]
            for g in 1:G
                lidx_g_c = lidxs[g, j]

                D_g_c = dp.D[g, j]
                m_g = 1 / ((h_c / (2 * D_g_c)) + 2)

                M[lidx_g_c, lidx_g_c] += m_g / h_c
            end
        end
    end

    return M
end

# NOTE : construct diffusion Q : inhomogeneous source
function const_diffus_Q(dp::DP)
    G = get_G(dp)
    J = get_J(dp)

    lidxs = LinearIndices((G, J))

    Q = zeros(G*J)

    for j in 1:J
        for g in 1:G
            lidx_g_c = lidxs[g, j]

            # NOTE : inhomogeneous source
            Q[lidx_g_c] += dp.Q[g, j]
        end
    end

    # boundary condition contribution

    for (j, b) in zip([1, J], [dp.lb, dp.rb])
        if isa(b, MDB)
            h_c = dp.h[j]
            for g in 1:G
                lidx_g_c = lidxs[g, j]

                D_g_c = dp.D[g, j]
                m_g = 1 / ((h_c / (2 * D_g_c)) + 2)

                Jm_g = b.Jm[g]
                Q[lidx_g_c] += (4 * m_g * Jm_g) / h_c
            end
        end
    end

    return Q
end

# NOTE : construct diffusion F : fission operator
function const_diffus_F(dp::DP)
    G = get_G(dp)
    J = get_J(dp)

    lidxs = LinearIndices((G, J))

    F = spzeros(G*J, G*J)

    for j in 1:J
        for g in 1:G
            lidx_g_c = lidxs[g, j]

            χ_g_c = dp.χ[g, j]

            for gp in 1:G
                lidx_gp_c = lidxs[gp, j]

                νΣf_gp_c = dp.νΣf[gp, j]

                F[lidx_g_c, lidx_gp_c] += χ_g_c * νΣf_gp_c
            end
        end
    end

    return F
end

# NOTE : construct the system M ϕ = Q
function const_diffus_fixed_sys(dp::DP)
    M = const_diffus_M(dp)
    Q = const_diffus_Q(dp)

    return M, Q
end

# NOTE : construct the system M ϕ = (λ) F ϕ
function const_diffus_eigen_sys(dp::DP)
    M = const_diffus_M(dp)
    F = const_diffus_F(dp)

    return M, F
end

# NOTE : generate relative ∞-norm convergence function
# converged when ||(n - o) / n||_{∞} < ϵ
function gen_rel_cf(ϵ::F; δ::F = ϵ * ϵ)
    function rel_cf(o, n)
        c = norm((@. (n - o) / (abs(n) + δ)), Inf) < ϵ
        return c
    end
    return rel_cf
end

function gen_rms_cf(ϵ::F)
    function rms_cf(o, n)
        l = length(o)
        c = sqrt((1 / l) * sum((n - o).^2)) < ϵ
        return c
    end
    return rms_cf
end

def_ϵ  = 1.e-10
def_cf = gen_rel_cf(def_ϵ)

def_mi = Inf

def_λs_0 = 0.

# NOTE : the version used in Ben Yee's MSED thesis
def_parcs_c0 = 0.02 # or 0.01 (0.02 chosen to be more conservative)
def_parcs_c1 = 10.
def_parcs_λ_min = 1 / 3.
gen_λs_fun_parcs(c0::F, c1::F, λ_min::F) = begin
    function λs_fun_parcs(λs_l::F, λ_l::F, λ_n::F)
        λs_n = max(λ_n - c1 * abs(λ_n - λ_l) - c0, λ_min)
        return λs_n
    end
    return λs_fun_parcs
end
# NOTE : the version used in the "Slab" code & Prof. Larsen's NERS 590 final
# project
def_slab_c0 = 0.99
def_slab_c1 = 10.
def_slab_λ_min = 0.
gen_λs_fun_slab(c0::F, c1::F, λ_min::F) = begin
    function λs_fun_slab(λs_l::F, λ_l::F, λ_n::F)
        λs_n = max(c0 * λ_n - c1 * abs(λ_n - λ_l), λ_min)
        return λs_n
    end
    return λs_fun_slab
end

function λs_fun_const(λs_l::F, λ_l::F, λ_n::F)
    return λs_l
end

def_λs_fun = gen_λs_fun_slab(def_slab_c0, def_slab_c1, def_slab_λ_min)

# NOTE : perform standard power iteration
# solution vector scaled so that it's 2-norm is unity
# L      : maximum iteration number
# cf     : convergence function cf(ϕ^{(l)}, ϕ^{(l+1)})
# ϕ_0    : initial dominant eigenvector guess
# λ_0    : initial dominant eigenvalue  guess
# λs_0   : initial λ-shift parameter
# λs_fun : function to determine new λ-shift
# λ_norm : norm used to determine new eigenvalue estimate each power iteration
# save   : save iterated values to determine spectral radius (used for testing)
function power_iterate(M, F; L = Inf, cf = def_cf,
                       ϕ_0  = def_vector_param(size(M,1)),
                       λ_0  = def_scalar_param(),
                       λs_0 = def_λs_0,
                       λs_fun = def_λs_fun,
                       λ_norm = 2,
                       save = false)
    if save
        ϕs = []
        λs = []
    end

    # NOTE : naming conventions
    # l → last
    # m → mid (before normaliz.)
    # n → next
    l = 0
    ϕ_l  = ϕ_0
    λ_l  = λ_0
    λs_l = λs_0
    while l < L
        f_l = F * ϕ_l
        ϕ_m = (M - λs_l * F) \ ((λ_l - λs_l) * f_l)
        λ_n = λs_l + (λ_l - λs_l) * norm(f_l, λ_norm) / norm(F * ϕ_m, λ_norm)
        ϕ_n = ϕ_m / norm(ϕ_m, 2)

        # update Wielandt shift (if iteration dependent)
        λs_n = λs_fun(λs_l, λ_l, λ_n)

        l += 1

        c = cf(ϕ_l, ϕ_n) && cf(λ_l, λ_n)

        if save
            push!(ϕs, ϕ_n)
            push!(λs, λ_n)
        end

        ϕ_l  = ϕ_n
        λ_l  = λ_n
        λs_l = λs_n

        c ? (@goto label_conv_power) : nothing
    end
    @label label_conv_power

    if save
        return ϕs, λs
    else
      return ϕ_l, λ_l
    end
end

abstract type TransportBoundary <: Boundary end
TB = TransportBoundary

# NOTE : ψm ordinate values should be ordered in the same manner as get_inc_quad_idxs(tp, e)
struct IncidentTransportBoundary <: TransportBoundary
    ψm::Array{F, 2} # N_{incoming, b} x G
end
ITB = IncidentTransportBoundary

struct ReflectiveTransportBoundary <: TransportBoundary end
RTB = ReflectiveTransportBoundary

# G : number of groups
# J : number of cells
# N : number of discrete ordinates
struct TransportProblem
    N :: Int
    K :: Int
    G :: Int
    J :: Int

    h   :: Array{F, 1} # J
    Σt  :: Array{F, 2} # G x J
    Σs  :: Array{F, 4} # K x G x G x J
    χ   :: Array{F, 2} # G x J
    νΣf :: Array{F, 2} # G x J
    Q   :: Array{F, 2} # G x J

    lb :: TB
    rb :: TB

    # angular quadrature ordinates, weights
    μ :: Array{F, 1} # N
    w :: Array{F, 1} # N

    # NOTE : DO NOT allow choice of quadrature set (sweeping / boundary indexing
    # assumptions are currently made on ordinate + weight layouts)
    function TransportProblem(N::Int,
                              K::Int,
                              G::Int,
                              J::Int,
                              h   :: Array{F, 1},
                              Σt  :: Array{F, 2},
                              Σs  :: Array{F, 4},
                              χ   :: Array{F, 2},
                              νΣf :: Array{F, 2},
                              Q   :: Array{F, 2},
                              lb :: TB,
                              rb :: TB)
        μ, w = gen_quad(N)

        return new(N, K, G, J,
                   h, Σt, Σs, χ, νΣf, Q,
                   lb, rb,
                   μ, w)
    end
end
TP = TransportProblem

# NOTE : TransportProblem objects treated as scalars
length(tp::TP) = 1
iterate(tp::TP)        = (tp, nothing)
iterate(tp::TP, ::Any) = nothing

# constructor for identical material properties in each spatial cell
function TransportProblem(N::Int, K::Int, G::Int, J::Int,
                          h   :: Array{F, 1},
                          Σt  :: Array{F, 1},
                          Σs  :: Array{F, 2},
                          χ   :: Array{F, 1},
                          νΣf :: Array{F, 1},
                          Q   :: Array{F, 1},
                          lb::TB, rb::TB)
    return TransportProblem(N, K, G, J,
                            h,
                            repeat(Σt ; outer=(1, J)),
                            reshape(repeat(vec(Σs); inner=K, outer=J), K, G, G, J),
                            repeat(χ  ; outer=(1, J)),
                            repeat(νΣf; outer=(1, J)),
                            repeat(Q  ; outer=(1, J)),
                            lb, rb)
end

# number of slab geometry boundaries
num_slab_bounds = 2

get_N(tp::TP)  = tp.N
get_K(tp::TP)  = tp.K
get_G(tp::TP)  = tp.G
# number cells
get_J(tp::TP)  = tp.J          # num cells
# number edges
get_E(tp::TP)  = get_J(tp) + 1 # num edges
# number boundary edges
get_Eb(tp::TP) = num_slab_bounds

# return left and right face indices (e_{l}, e_{r}) ∈ {1...E} for the given cell
# index j
function get_e_f(tp::TP, j::Int)
    e_l = j
    e_r = j + 1
    return e_l, e_r
end

# TODO : add more than Diamond Difference
function get_α(tp::TP, n::Int, g::Int, j::Int)
    return 0.
end

gen_quad(N::Int) = begin
    isodd(N) ? error("quad. set must of even order") : nothing
    μ, w = gausslegendre(N)
    return μ, w
end

rg_id = 1 # right-going hemisphere i.d. (μ > 0)
lg_id = 2 # left -going hemisphere i.d. (μ < 0)
struct HemisphereID
    id::Int

    HemisphereID(id::Int) = begin
        if ! (id in [lg_id, rg_id])
            error("invalid hemisphere i.d.")
        else
            new(id)
        end
    end
end
HID = HemisphereID
is_rg(hid::HID) = hid.id == rg_id
is_lg(hid::HID) = hid.id == lg_id

rg_hid = HID(rg_id)
lg_hid = HID(lg_id)

# NOTE : hemi → hemisphere (i.d. = 1 → μ > 0, i.d. = 2 → μ < 0)
get_hemi_quad_idxs(tp::TP, hid::HID) = begin
    N = get_N(tp)
    if     is_rg(hid)
        return (div(N,2) + 1):1:N
    elseif is_lg(hid)
        return div(N,2):-1:1
    end
end

# NOTE : idxs returned in "reflective" ordering
# inc : incoming !INTO! domain
# out : outgoing !FROM! domain
get_inc_quad_idxs(tp::TP, e::Int) = begin
    E = get_E(tp)
    if     e == 1
        get_hemi_quad_idxs(tp, rg_hid)
    elseif e == E
        get_hemi_quad_idxs(tp, lg_hid)
    else
        error("cell $(j) is not a boundary cell ($(j) ∉ {1, $(J)})")
    end
end
get_out_quad_idxs(tp::TP, e::Int) = begin
    E = get_E(tp)
    if     e == 1
        get_hemi_quad_idxs(tp, lg_hid)
    elseif e == E
        get_hemi_quad_idxs(tp, rg_hid)
    else
        error("cell $(j) is not a boundary cell ($(j) ∉ {1, $(J)})")
    end
end

get_march_graph(tp::TP, hid::HID) = begin
    J = get_J(tp)

    if     hid == rg_hid
        return 1:1:J
    elseif hid == lg_hid
        return J:-1:1
    end
end

get_march_prec(tp::TP) = ((isa(tp.lb, RTB) && isa(tp.rb, ITB))
                          ? [lg_hid, rg_hid]
                          : [rg_hid, lg_hid])

function get_inc_b(tp::TP, hid::HID)
    if     hid == rg_hid
        return tp.lb
    elseif hid == lg_hid
        return tp.rb
    end
end

# returns boundary condition structs and their associated edge indices
function get_e_b(tp::TP)
    E = get_E(tp)
    return zip([1, E], [tp.lb, tp.rb])
end

# construct transport D : discrete → moment operator
# (K*G*J) x (N*G*J)
function const_transp_D(tp::TP; K = get_K(tp))
    N = get_N(tp)
    G = get_G(tp)
    J = get_J(tp)

    lidxs_row = LinearIndices((K, G, J))
    lidxs_col = LinearIndices((N, G, J))

    D = spzeros(K*G*J, N*G*J)

    for j in 1:J
        for g in 1:G
            for k in 1:K
                for n in 1:N
                    D[lidxs_row[k, g, j], lidxs_col[n, g, j]] += (
                        legendre(tp.μ[n], k-1) * tp.w[n]
                    )
                end
            end
        end
    end

    return D
end

# construct transport S : scattering operator
# (K*G*J) x (K*G*J)
function const_transp_S(tp::TP)
    K = get_K(tp)
    G = get_G(tp)
    J = get_J(tp)

    lidxs_row = LinearIndices((K, G, J))
    lidxs_col = LinearIndices((K, G, J))

    S = spzeros(K*G*J, K*G*J)

    for j in 1:J
        for g in 1:G
            for gp in 1:G
                for k in 1:K
                    S[lidxs_row[k,g,j], lidxs_col[k,gp,j]] += (
                        tp.Σs[k, gp, g, j]
                    )
                end
            end
        end
    end

    return S
end

function const_transp_F(tp::TP)
    G = get_G(tp)
    J = get_J(tp)

    lidxs_row = LinearIndices((G, J))
    lidxs_col = LinearIndices((G, J))

    F = spzeros(G*J, G*J)

    for j in 1:J
        for g in 1:G
            for gp in 1:G
                F[lidxs_row[g, j], lidxs_col[gp, j]] += tp.χ[g,j] * tp.νΣf[gp,j]
            end
        end
    end

    return F
end

# construct transport M : moment → discrete operator
# (N*G*J) x (K*G*J)
function const_transp_M(tp::TP; K = get_K(tp))
    N = get_N(tp)
    G = get_G(tp)
    J = get_J(tp)

    lidxs_row = LinearIndices((N, G, J))
    lidxs_col = LinearIndices((K, G, J))

    M = spzeros(N*G*J, K*G*J)

    for j in 1:J
        for g in 1:G
            for n in 1:N
                for k in 1:K
                      M[lidxs_row[n,g,j], lidxs_col[k,g,j]] += (
                          ((2 * (k-1) + 1) / 2.) * legendre(tp.μ[n], k-1)
                      )
                end
            end
        end
    end

    return M
end

# # NOTE construct transport L : leakage + collision operator
# # (N*G*J x N*G*E)
function const_transp_L(tp::TP)
    N = get_N(tp)
    G = get_G(tp)
    J = get_J(tp)
    E = get_E(tp)

    lidxs_row = LinearIndices((N, G, J))
    lidxs_col = LinearIndices((N, G, E))

    L = spzeros(N*G*J, N*G*E)

    # N * G * J cell-average equations
    for j in 1:J
        e_l, e_r = get_e_f(tp, j)

        h_j = tp.h[j]
        for g in 1:G
            Σt_g_j = tp.Σt[g, j]
            for n in 1:N
                lidx_row = lidxs_row[n, g, j]
                lidx_n_g_e_r = lidxs_col[n, g, e_r]
                lidx_n_g_e_l = lidxs_col[n, g, e_l]

                μ_n     = tp.μ[n]
                α_n_g_j = get_α(tp, n, g, j)

                L[lidx_row, lidx_n_g_e_r] += (
                    + (μ_n / h_j) + Σt_g_j * (1. + α_n_g_j) / 2.
                )
                L[lidx_row, lidx_n_g_e_l] += (
                    - (μ_n / h_j) + Σt_g_j * (1. - α_n_g_j) / 2.
                )
            end
        end
    end

    return L
end

# NOTE construct transport Lb : incoming angular flux operator
# (N*G x N*G*E)
function const_transp_Lb(tp::TP)
    N  = get_N(tp)
    G  = get_G(tp)
    E  = get_E(tp)
    Eb = get_Eb(tp)

    N_inc = div(N, 2)

    lidxs_row = LinearIndices((N_inc, G, Eb))
    lidxs_col = LinearIndices((N    , G, E ))

    Lb = spzeros(N_inc*G*Eb, N*G*E)

    for (r_e, (e, b)) in enumerate(get_e_b(tp))
        if isa(b, RTB)
            for g in 1:G
                for (r_n, (n_i, n_o)) in enumerate(zip(get_inc_quad_idxs(tp, e),
                                                       get_out_quad_idxs(tp, e)))
                    Lb[lidxs_row[r_n, g, r_e], lidxs_col[n_i, g, e]] += + 1.
                    Lb[lidxs_row[r_n, g, r_e], lidxs_col[n_o, g, e]] += - 1.
                end
            end
        elseif isa(b, ITB)
            for g in 1:G
                for (r_n, n_i) in enumerate(get_inc_quad_idxs(tp, e))
                    Lb[lidxs_row[r_n, g, r_e], lidxs_col[n_i, g, e]] += + 1.
                end
            end
        end
    end

    return Lb
end

# construct transport Q : inhomogeneous source
# (N*G*J)
# NOTE : enforces isotropic inhomogeneous source
function const_transp_Q(tp::TP)
    N = get_N(tp)
    G = get_G(tp)
    J = get_J(tp)

    lidxs = LinearIndices((N,G,J))

    Q = zeros(N*G*J)


    for j in 1:J
        for g in 1:G
            for n in 1:N
                # k = 1
                Q[lidxs[n,g,j]] += (1 / 2) * tp.Q[g, j]
            end
        end
    end

    return Q
end
function const_transp_Qb(tp::TP)
    N  = get_N(tp)
    G  = get_G(tp)
    Eb = get_Eb(tp)

    N_inc = div(N,2)

    lidxs = LinearIndices((N_inc, G, Eb))

    Qb = zeros(N_inc*G*Eb)

    for (r_e, (e, b)) in enumerate(get_e_b(tp))
        if isa(b, ITB)
            for g in 1:G
                for (r_n, n_i) in enumerate(get_inc_quad_idxs(tp, e))
                    Qb[lidxs[r_n, g, r_e]] += b.ψm[r_n, g]
                end
            end
        end
    end

    return Qb
end

# NOTE  construct transport C : edge → center operator (rectangular)
# (N*G*J x N*G*E)
function const_transp_C(tp::TP)
    N = get_N(tp)
    G = get_G(tp)
    J = get_J(tp)
    E = get_E(tp)

    lidxs_row = LinearIndices((N, G, J))
    lidxs_col = LinearIndices((N, G, E))

    C = spzeros(N*G*J, N*G*E)

    for j in 1:J
        e_l, e_r = get_e_f(tp, j)
        for g in 1:G
            for n in 1:N
                lidx_row = lidxs_row[n, g, j]
                lidx_n_g_e_r = lidxs_col[n, g, e_r]
                lidx_n_g_e_l = lidxs_col[n, g, e_l]

                α_n_g_j = get_α(tp, n, g, j)

                C[lidxs_row[n,g,j], lidxs_col[n,g,e_r]] += (1. + α_n_g_j) / 2.
                C[lidxs_row[n,g,j], lidxs_col[n,g,e_l]] += (1. - α_n_g_j) / 2.
            end
        end
    end

    return C
end

# NOTE : construct system [(L - (M*S*D + M_1*F*D_1)*C); Lb] ψ = [Q; Qb]
# return system of the form T ψ = Q
function const_transp_fixed_sys(tp::TP)
    L  = const_transp_L(tp)
    Lb = const_transp_Lb(tp)
    S  = const_transp_S(tp)
    F  = const_transp_F(tp)

    Qv = const_transp_Q(tp)
    Qb = const_transp_Qb(tp)

    C = const_transp_C(tp)

    # discrete → moment
    D   = const_transp_D(tp)
    D_1 = const_transp_D(tp; K = 1)

    # moment → discrete
    M   = const_transp_M(tp)
    M_1 = const_transp_M(tp; K = 1)

    T = [(L - (M*S*D + M_1*F*D_1)*C); Lb]
    Q = [Qv; Qb]

    return T, Q
end

# NOTE : construct system R ψ = λ U ψ
# R = [L - M*S*D*C; Lb]
# U = [M_1*F*D_1*C; ... 0 ...]
function const_transp_eig_sys(tp::TP)
    L  = const_transp_L(tp)
    Lb = const_transp_Lb(tp)
    S  = const_transp_S(tp)
    F  = const_transp_F(tp)

    C = const_transp_C(tp)

    # discrete → moment
    D   = const_transp_D(tp)
    D_1 = const_transp_D(tp; K = 1)

    # moment → discrete
    M   = const_transp_M(tp)
    M_1 = const_transp_M(tp; K = 1)

    R = [L - M*S*D*C; Lb]
    U = [M_1*F*D_1*C; spzeros(size(Lb)...)]

    return R, U
end

# NOTE : default normalization function (eigenfunction)
def_nsf = (ϕs) -> (1 / norm(ϕs, 2))

include("Compat.jl")

march_rg_dd(μs::VF, h::F, Σt::F, ψsi::VF, Sn::VF) = begin
    α = 0. # TODO : extend beyond DD
    αp = (1 + α) / 2
    αm = (1 - α) / 2

    ψso = @. ((μs - αm*Σt*h)*ψsi + Sn*h) / (μs + Σt*h*αp)
    ψsa = @. αp*ψso + αm*ψsi

    return ψsi, ψsa, ψso
end
march_lg_dd(μs::VF, h::F, Σt::F, ψsi::VF, Sn::VF) = begin
    α = 0. # TODO : extend beyond DD
    αp = (1 + α) / 2
    αm = (1 - α) / 2

    ψso = @. ((-μs - αp*Σt*h)*ψsi + Sn*h)  / (-μs + Σt*h*αm)
    ψsa = @. αp*ψsi + αm*ψso

    return ψsi, ψsa, ψso
end

gen_march_fun(tp::TP, hid::HID) = begin
    if is_rg(hid)
        return march_rg_dd
    elseif is_lg(hid)
        return march_lg_dd
    end
end

gen_ψsb_out_e(tp::TP) = zeros(div(get_N(tp),2), get_G(tp))

expand_legendre_moments(μs::VF, Sk::VF) = begin
    N = length(μs)
    K = length(Sk)

    Sn = zeros(N)
    for (n, μ) in enumerate(μs)
        for (k, sk) in zip(0:(K-1), Sk)
            Sn[n] += (2*k + 1) / (2) * legendre(μ, k) * sk
        end
    end

    return Sn
end
compute_legendre_moment(k::Int, μs::VF, ws::VF, ψ::VF) = begin
    return sum(@. legendre(μs, k) * ψ * ws)
end

get_e_inc(tp::TP, j::Int, hid::HID) = begin
    if     is_rg(hid)
        return j
    elseif is_lg(hid)
        return j + 1
    end
end
get_e_out(tp::TP, j::Int, hid::HID) = begin
    if is_rg(hid)
        return j + 1
    elseif is_lg(hid)
        return j
    end
end

glmf = (mom::Int) -> begin
    (μs::VF, ws::VF, ψ::VF) -> begin
        return compute_legendre_moment(mom, μs, ws, ψ)
    end
end
bcf = (μs::VF, ws::VF, ψ::VF) -> begin
    return sum(@. abs(μs * ψ * ws))
end

def_cent_sfs = [glmf(0)]
def_edge_sfs = [glmf(1), bcf]

# Sk : source moments (K x G x J)
# NOTE : cent_sfs & edge_sfs MUST return a float
function transport_sweep(tp::TP, Sk::Array{F,3};
                         cent_sfs::VFun = def_cent_sfs,
                         edge_sfs::VFun = def_edge_sfs,
                         ψ_out_b_est::Union{MF, Nothing} = nothing)
    N = get_N(tp)
    G = get_G(tp)
    J = get_J(tp)
    E = get_E(tp)

    march_hids = get_march_prec(tp)

    n_c = length(cent_sfs)
    n_e = length(edge_sfs)
    cent_sds = zeros(n_c, G, J)
    edge_sds = zeros(n_e, G, E)

    # outgoing boundary angular flux initial estimate
    ψ_out_b = ψ_out_b_est
    for hid in march_hids
        # incoming boundary
        inc_b = get_inc_b(tp, hid)

        if isa(inc_b, ITB)
            ψ_inc_b = inc_b.ψm
        elseif isa(inc_b, RTB)
            ψ_inc_b = ψ_out_b
        end

        quad_idxs = get_hemi_quad_idxs(tp, hid)
        μs = tp.μ[quad_idxs]
        ws = tp.w[quad_idxs]

        js = get_march_graph(tp, hid)

        mf = gen_march_fun(tp, hid)

        # NOTE : need to process incoming boundary edge
        e_inc_b = get_e_inc(tp, first(js), hid)
        for g in 1:G
            for (idx_e_sf, e_sf) in enumerate(edge_sfs)
                edge_sds[idx_e_sf, g, e_inc_b] += (
                    e_sf(μs, ws, ψ_inc_b[:,g])
                )
            end
        end

        ψ_inc_c = ψ_inc_b
        for j in js
            h_j = tp.h[j]
            e_out = get_e_out(tp, j, hid)

            ψ_out_c = zeros(div(N,2), G)
            for g in 1:G
                Σt_g_j    = tp.Σt[g,j]
                Sn_g_j    = expand_legendre_moments(μs, Sk[:,g,j])
                ψ_inc_c_g = ψ_inc_c[:, g]
                _ψ_inc_c_g, ψ_avg_c_g, ψ_out_c_g = mf(μs, h_j, Σt_g_j, ψ_inc_c_g, Sn_g_j)

                for (idx_c_sf, c_sf) in enumerate(cent_sfs)
                    cent_sds[idx_c_sf, g, j]     += c_sf(μs, ws, ψ_avg_c_g)
                end
                for (idx_e_sf, e_sf) in enumerate(edge_sfs)
                    edge_sds[idx_e_sf, g, e_out] += e_sf(μs, ws, ψ_out_c_g)
                end

                # NOTE : overwrite
                ψ_out_c[:, g] = ψ_out_c_g
            end

            ψ_inc_c = ψ_out_c
        end

        ψ_out_b = ψ_inc_c
    end

    return cent_sds, edge_sds
end

def_Φs(tp::TP) = begin
    K = get_K(tp)
    G = get_G(tp)
    J = get_J(tp)

    return reshape(repeat(def_vector_param(J), inner=K*G), K, G, J)
end

function skp_calc(tp, Φ)
    K = get_K(tp)
    G = get_G(tp)
    J = get_J(tp)

    od = zeros(K,G,J)

    # scattering source
    for j in 1:J
        for g in 1:G
            for gp in 1:G
                for k in 1:K
                    od[k, g, j] += tp.Σs[k, gp, g, j] * Φ[k, gp, j]
                end
            end
        end
    end

    # inhomogeneous source
    for j in 1:J
        for g in 1:G
            od[1, g, j] += tp.Q[g, j]
        end
    end

    # fission source
    for j in 1:J
        for g in 1:G
            for gp in 1:G
                od[1, g, j] += tp.χ[g,j] * tp.νΣf[gp,j] * Φ[1, gp, j]
            end
        end
    end

    return od
end

# TODO : eigenvalue
function source_iteration(tp::TP;
                          mi::Number       = def_mi,
                          cf::Function     = def_cf,
                          Φs_0::Array{F,3} = def_Φs(tp))
    K = get_K(tp)

    leg_mom_sfs = glmf.(0:1:(K-1))

    Φss = Array{Array{F,3},1}(undef, 0)

    it = 0 # num. completed iterations
    Φs_p = Φs_0
    while true
        Sk_p = skp_calc(tp, Φs_p)
        Φs_n, _ = transport_sweep(tp, Sk_p;
                                  cent_sfs = leg_mom_sfs)

        it += 1

        converged     = cf(Φs_p, Φs_n)
        iters_reached = it >= mi

        push!(Φss, Φs_n)

        Φs_p = Φs_n

        converged     ? (@goto label_converged)     : nothing
        iters_reached ? (@goto label_iters_reached) : nothing
    end
    @label label_converged
    @label label_iters_reached

    return Φss
end

end # module
